<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('/payroll');
    // return view('welcome');
});

Route::get('/payroll', function () {
    return view('payroll.index');
});

Route::get('/payrate', function () {
    return view('payrate.index');
});

Route::get('/payrate/create', function () {
    return view('payrate.create');
});

Route::get('/payrate/edit', function () {
    return view('payrate.edit');
});

Route::get('/billrate', function () {
    return view('billrate.index');
});

Route::get('/billrate/create', function () {
    return view('billrate.create');
});

Route::get('/billrate/edit', function () {
    return view('billrate.edit');
});

Route::get('/invoice', function () {
    return view('invoice.index');
});

Route::get('/invoice/progressbar', function () {
    return view('invoice.progressbar');
});

Route::get('/dashboard', function () {
    return view('dashboard.index');
});