@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 pt-6 pb-8">
        <div class="flex justify-between mb-3">
            <input type="search" name="serch" placeholder="Search" class="bg-white border border-gray-400 h-8 px-5 pr-10 text-sm focus:outline-none"> 
            <div class="flex text-purple-700">
                <button class="bg-purple-700 hover:bg-purple-800 text-white shadow px-6 flex items-center">
                    <svg class="fill-current h-4 w-4" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 512c-141.164062 0-256-114.835938-256-256s114.835938-256 256-256 256 114.835938 256 256-114.835938 256-256 256zm0-480c-123.519531 0-224 100.480469-224 224s100.480469 224 224 224 224-100.480469 224-224-100.480469-224-224-224zm0 0"/><path d="m368 272h-224c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h224c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path d="m256 384c-8.832031 0-16-7.167969-16-16v-224c0-8.832031 7.167969-16 16-16s16 7.167969 16 16v224c0 8.832031-7.167969 16-16 16zm0 0"/></svg>
                    <span class="ml-2">Create</span>
                </button>  
            </div>      
        </div>
        <div class="bg-white border">
            <table class="table-auto w-full">
                <thead>
                    <tr class="text-left text-xs bg-gray-400">
                        <th class="font-medium px-2 py-2">ID</th>
                        <th class="font-medium px-2 py-1">Client</th>
                        <th class="font-medium px-2 py-1">Temp</th>
                        <th class="font-medium px-2 py-1">Pay Rate</th>
                        <th class="font-medium px-2 py-1">Bill Rate</th>
                        <th class="font-medium px-2 py-1">Shift</th>
                        <th class="font-medium px-2 py-1">Skill</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="text-xs">                        
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">wordproccessor</td>
                    </tr>
                    <tr class="text-xs">                        
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">wordproccessor</td>
                    </tr>
                    <tr class="text-xs">                        
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">wordproccessor</td>
                    </tr>
                    <tr class="text-xs">                        
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">wordproccessor</td>
                    </tr>
                    <tr class="text-xs">                        
                        <td class="border px-1 py-3"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                    </tr>                                                                                                                                                                                              
                </tbody>
            </table>            
        </div>
    </div>
@endsection