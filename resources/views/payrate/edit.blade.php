@extends('layouts.app')

@section('content')
	<div class="container mx-auto pt-6">
		<div class="flex justify-center">
			<div class="bg-white border w-1/4 px-6 py-6 relative text-gray-800">
				<div class="text-gray-500 hover:text-gray-800 z-20 absolute top-0 right-0 p-2">
					<svg class="h-4 w-4 fill-current"  viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>
				</div>
				<div class="text-center text-xl">Edit temp</div>
				<div class="text-center pb-4 text-sm text-gray-700">Temp name</div>
				<div class="flex items-center justify-between mb-3">
					<div>Pay Rate</div>
					<div class="relative py-2 border border-gray-400 hover:border-purple-800 w-2/3">
						<div class="absolute pl-2 flex items-center">
							<span class="text-sm leading-5">
								$
							</span>
						</div>
						<input class="form-input focus:outline-none flex items-center pl-6 text-sm leading-5" placeholder="0.00" />
					</div>
				</div>
				<div class="flex items-center justify-between mb-3">
					<div>Bill Rate</div>
					<div class="relative py-2 border border-gray-400 hover:border-purple-800 w-2/3">
						<div class="absolute pl-2 flex items-center">
							<span class="text-sm leading-5">
								$
							</span>
						</div>
						<input class="form-input focus:outline-none flex items-center pl-6 text-sm leading-5" placeholder="0.00" />
					</div>
				</div>
				<div class="flex items-center justify-between mb-3">
					<div>Shift</div>
					<div class="relative w-2/3">
						<select class="appearance-none w-full bg-white rounded-none border border-gray-400 hover:border-purple-800 px-4 py-2 text-sm focus:outline-none">
							<option>1</option>
							<option>2</option>
							<option>3</option>
						</select>
						<div class="absolute inset-y-0 right-0 flex items-center px-2 text-gray-800">
							<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
						</div>
					</div>
				</div>
				<div class="flex items-center justify-between mb-3">
					<div>Skill</div>
					<div class="relative w-2/3">
						<select class="appearance-none w-full bg-white rounded-none border border-gray-400 hover:border-purple-800 px-4 py-2 text-sm focus:outline-none">
							<option>wordproccessor</option>
							<option>wordproccessor</option>
							<option>wordproccessor</option>
						</select>
						<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
							<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
						</div>
					</div>
				</div>
				<div class="flex justify-between pt-3">
					<button class="w-32 bg-red-500 hover:bg-red-700 text-white shadow py-1 px-3">
						Delete
					</button>
					<button class="w-32 bg-purple-700 hover:bg-purple-800 text-white shadow py-1 px-6">
						Edit
					</button>
				</div>
			</div>
		</div>
	</div>
@endsection