<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>{{ config('app.name', 'Timesheet') }}</title>

    <script>
        window.App = {!! json_encode([
            'user' => \Illuminate\Support\Facades\Auth::user(),
        ]) !!};
    </script>

    <style>
      .rounded-lg{
        border-radius: .5rem !important;
      }

      .v--modal {
        border-radius: .5rem !important;
      }
    </style>
  </head>
  <body class="font-sans bg-gray-200">
    <div id="app">
      <!-- navigation -->
      <div>
        <!-- top nav -->
        @include('layouts.header')

        <!-- secondary nav -->
        @include('layouts.sub_menu')
      </div>

      @yield('content')
    </div>
  </body>
  <script src="{{ mix('js/app.js') }}"></script>
</html>
