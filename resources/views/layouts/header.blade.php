<div class="bg-purple-700 text-white">
    <div class="container mx-auto px-4">
    <div class="flex items-center py-4 justify-between">
        <div class="text-xl">
        Spreadsheet
        </div>
        
        <!--<div>
            <a href="#" class="relative block">
            <svg xmlns="http://www.w3.org/2000/svg" class="fill-current text-white h-8 w-8 block" viewBox="0 0 512 512"><path d="M255.9 456c31.1 0 48.1-22 48.1-53h-96.3c0 31 17 53 48.2 53zM412 352.2c-15.4-20.3-45.7-32.2-45.7-123.1 0-93.3-41.2-130.8-79.6-139.8-3.6-.9-6.2-2.1-6.2-5.9v-2.9c0-13.4-11-24.7-24.4-24.6-13.4-.2-24.4 11.2-24.4 24.6v2.9c0 3.7-2.6 5-6.2 5.9-38.5 9.1-79.6 46.5-79.6 139.8 0 90.9-30.3 102.7-45.7 123.1-9.9 13.1-.5 31.8 15.9 31.8h280.1c16.3 0 25.7-18.8 15.8-31.8z"/></svg>
            <span class="rounded-full w-4 h-4 absolute pin-r pin-t bg-red-dark text-white text-center text-sm">
            <small>3</small>
            </span>
            </a>
        </div>-->
        <div class="flex items-center">
            <div class="text-white">
            <svg class="h-8 w-8 fill-current" xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'><title>ionicons-v5-j</title><path d='M258.9,48C141.92,46.42,46.42,141.92,48,258.9,49.56,371.09,140.91,462.44,253.1,464c117,1.6,212.48-93.9,210.88-210.88C462.44,140.91,371.09,49.56,258.9,48ZM385.32,375.25a4,4,0,0,1-6.14-.32,124.27,124.27,0,0,0-32.35-29.59C321.37,329,289.11,320,256,320s-65.37,9-90.83,25.34a124.24,124.24,0,0,0-32.35,29.58,4,4,0,0,1-6.14.32A175.32,175.32,0,0,1,80,259C78.37,161.69,158.22,80.24,255.57,80S432,158.81,432,256A175.32,175.32,0,0,1,385.32,375.25Z'/><path d='M256,144c-19.72,0-37.55,7.39-50.22,20.82s-19,32-17.57,51.93C191.11,256,221.52,288,256,288s64.83-32,67.79-71.24c1.48-19.74-4.8-38.14-17.68-51.82C293.39,151.44,275.59,144,256,144Z'/></svg>
            </div>
            <div class="block flex items-center ml-2">
            <span class="text-sm mr-1">Ravshan</span>
            <a  href="#"
                onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
                <svg xmlns="http://www.w3.org/2000/svg" class="fill-current text-white h-4 w-4 block" viewBox="0 0 520 520">
                <path d="M192 277.4h189.7l-43.6 44.7L368 352l96-96-96-96-31 29.9 44.7 44.7H192v42.8z"/><path d="M255.7 421.3c-44.1 0-85.5-17.2-116.7-48.4-31.2-31.2-48.3-72.7-48.3-116.9 0-44.1 17.2-85.7 48.3-116.9 31.2-31.2 72.6-48.4 116.7-48.4 44 0 85.3 17.1 116.5 48.2l30.3-30.3c-8.5-8.4-17.8-16.2-27.7-23.2C339.7 61 298.6 48 255.7 48 141.2 48 48 141.3 48 256s93.2 208 207.7 208c42.9 0 84-13 119-37.5 10-7 19.2-14.7 27.7-23.2l-30.2-30.2c-31.1 31.1-72.5 48.2-116.5 48.2zM448.004 256.847l-.849-.848.849-.849.848.849z"/>
                </svg>
            </a>
            </div>
        </div>
    </div>
    </div>
</div>