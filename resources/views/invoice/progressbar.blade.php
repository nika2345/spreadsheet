@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 pt-3 pb-8">
        <div class="my-4">	
            <div class="flex pb-3">
                <div class="flex-1">
                 </div>
                <div class="flex-1">
                    <div class="w-10 h-10 bg-purple-700 mx-auto rounded-full text-lg flex items-center">
                         <span class="text-white text-center w-full">1</span>
                    </div>
                 </div>
                <div class="w-1/6 items-center flex">
                     <div class="w-full bg-purple-700 rounded items-center flex-1">
                         <div class="bg-purple-700 py-1 rounded w-full"></div>
                     </div>
                </div>
                <div class="flex-1">
                    <div class="w-10 h-10 bg-purple-700 mx-auto rounded-full text-lg flex items-center">
                         <span class="text-white text-center w-full">2</span>
                    </div>
                </div>
                <div class="w-1/6 items-center flex">
                    <div class="w-full bg-gray-500 rounded items-center flex-1">
                        <div class="bg-purple-700 py-1 rounded w-1/5"></div>
                    </div>
                </div>
                <div class="flex-1">
                    <div class="w-10 h-10 bg-gray-500 mx-auto rounded-full text-lg flex items-center">
                         <span class="text-white text-center w-full">3</span>
                    </div>
                </div>
                <div class="w-1/6 items-center flex">
                    <div class="w-full bg-gray-500 rounded items-center flex-1">
                        <div class="bg-purple-700 py-1 rounded w-0"></div>
                    </div>
                </div>
                <div class="flex-1">
                    <div class="w-10 h-10 bg-gray-500 mx-auto rounded-full text-lg flex items-center">
                         <span class="text-white text-center w-full">4</span>
                    </div>
                </div>
                <div class="flex-1">
                </div>		
            </div>

            <div class="flex text-xs text-center">
                <div class="w-1/4">
                    1 step
                </div>
                <div class="w-1/4">
                    2 step
                </div>
                <div class="w-1/4">
                    3 step
                </div>
                <div class="w-1/4">
                    4 step
                </div>			
            </div>
        </div>
    </div>      
@endsection