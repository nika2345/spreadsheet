@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 pt-3 pb-8 w-full">
       <div class="flex mt-3">
            <div class="w-3/4">
                <div class="flex mb-16">
                    <div class="mr-3">
                        <label class="text-sm font-medium text-gray-800">Client</label>
                        <div class="relative">
                            <select class="appearance-none w-64 bg-white rounded-none border border-gray-400 hover:border-purple-800 px-4 py-2 text-sm focus:outline-none">
                                <option>Debevoise</option>
                                <option>Debevoise</option>
                                <option>Debevoise</option>
                            </select>
                            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label class="text-sm font-medium text-gray-800">Client email</label>
                        <div class="w-64 bg-white py-2 border border-gray-400 hover:border-purple-800">
                            <input class="form-input focus:outline-none flex items-center pl-3 text-sm" placeholder="Birds@intuit.com" />
                        </div>
                    </div>
                </div>
                <div class="flex mb-3 mb-6">
                    <div class="mr-3">
                        <label class="text-sm font-medium text-gray-800">Billing address</label>
                        <div class="w-64 h-24 bg-white py-2 border border-gray-400 hover:border-purple-800">
                            <input class="form-input focus:outline-none flex items-center pl-3 text-sm" placeholder="Amy Lauterbach" />
                        </div>
                    </div>
                    <div class="mr-3">
                        <label class="text-sm font-medium text-gray-800">Terms</label>
                        <div class="relative">
                            <select class="appearance-none w-48 bg-white rounded-none border border-gray-400 hover:border-purple-800 px-4 py-2 text-sm focus:outline-none">
                                <option>Net 30</option>
                                <option>Net 30</option>
                                <option>Net 30</option>
                            </select>
                            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-900">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                    <div class="mr-3">
                        <label class="text-sm font-medium text-gray-800">Invoice date</label>
                        <div class="w-48 bg-white py-2 border border-gray-400 hover:border-purple-800">
                            <input class="form-input focus:outline-none flex items-center pl-3 text-sm" placeholder="04/01/2020" />
                        </div>
                    </div>
                    <div>
                        <label class="text-sm font-medium text-gray-800">Due date</label>
                        <div class="w-48 bg-white py-2 border border-gray-400 hover:border-purple-800">
                            <input class="form-input focus:outline-none flex items-center pl-3 text-sm" placeholder="04/01/2020" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-1/4 text-right">
                <div class="text-gray-600 font-medium uppercase text-sm">Balance Due</div>
                <div class="text-5xl">$812.50</div>
            </div>
        </div>
        <div class="bg-white border">
            <table class="table-fixed w-full">
                <thead>
                    <tr class="text-left text-xs bg-gray-400">
                        <th class="font-medium px-2 py-2 w-8">ID</th>
                        <th class="font-medium px-2 py-1">Client</th>
                        <th class="font-medium px-2 py-1">Temp</th>
                        <th class="font-medium px-2 py-1">Rate</th>
                        <th class="font-medium px-2 py-1">Amount</th>
                        <th class="font-medium px-2 py-1">TAX</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="text-xs"> 
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                    </tr>
                    <tr class="text-xs">
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">1</td>
                    </tr>
                    <tr class="text-xs"> 
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">1</td>
                    </tr>
                    <tr class="text-xs">    
                        <td class="border px-1 py-1">1</td>
                        <td class="border px-1 py-1">Debevoise</td>
                        <td class="border px-1 py-1">Doris Bruntel 2</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">$19,00</td>
                        <td class="border px-1 py-1">1</td>
                    </tr>
                    <tr class="text-xs">   
                        <td class="border px-1 py-3"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                        <td class="border px-1 py-1"></td>
                    </tr>                                                                                                                                                                                              
                </tbody>
            </table>            
        </div>
    </div>
@endsection