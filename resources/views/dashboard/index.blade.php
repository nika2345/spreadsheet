@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-10 pt-1 pb-8">
        <div class="flex">
            <div class="flex flex-wrap w-2/3">
                <div class="w-1/2 p-1">
                    <div class="bg-white border p-6 text-gray-700">
                        <div class="flex justify-between items-center">
                            <div>Profit and Loss</div>
                            <div class="flex">
                                <div class="text-xs text-gray-500">Last month</div>
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                        <div class="text-gray-500 mt-2 text-2xl">$-387</div>
                        <div class="uppercase text-gray-500 text-sm">Net profit for january</div>
                        <div class="py-12">
                            <div class="flex items-center">
                                <div class="w-2/3">
                                    <div class="w-full bg-gray-300">
                                        <div class="bg-green-500 py-3 w-2/3"></div>
                                    </div>
                                </div>
                                <div class="leading-tight w-1/3 pl-3">
                                    <div class="text-gray-500">$9,521</div>
                                    <div class="uppercase text-gray-800 text-xs">Income</div>
                                </div>
                            </div>
                            <div class="flex items-center mt-4">
                                <div class="w-2/3">  
                                    <div class="w-full bg-gray-300">
                                        <div class="bg-purple-700 py-3 w-3/4"></div>
                                    </div>
                                </div>
                                <div class="leading-tight w-1/3 pl-3">
                                    <div class="text-gray-500">$9,521</div>
                                    <div class="uppercase text-gray-800 text-xs">Expenses</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-1/2 p-1">
                    <div class="bg-white border h-64">1</div>
                </div>
                <div class="w-1/2 p-1">
                    <div class="bg-white border h-64">1</div>
                </div>
                <div class="w-1/2 p-1">
                    <div class="bg-white border h-64">1</div>
                </div>
            </div>
            <div class="w-1/3 p-1 flex flex-grow">
                <div class="bg-white border w-full">1</div>
            </div>
        </div>
    </div>
@endsection