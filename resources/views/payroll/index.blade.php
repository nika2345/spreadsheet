@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 pt-6 pb-8">
        <div class="flex justify-between">
            <input type="search" name="serch" placeholder="Search" class="bg-white border border-gray-400 h-8 px-5 pr-10 text-sm focus:outline-none"> 
            <div class="border-b">
                <div class="flex justify-end -mb-mx">
                    <div class="flex text-gray-500">
                        <button class="appearance-none text-xl font-medium text-purple-700 border-b py-2 border-transparent hover:border-blue-900 mr-4">
                            4/12/20
                        </button>
                        <button class="appearance-none border-b py-2 border-transparent hover:border-blue-900 mr-4">
                            4/5/20
                        </button>
                        <button class="appearance-none border-b py-2 border-transparent hover:border-blue-900 mr-4">
                            3/29/20
                        </button>
                        <button class="appearance-none border-b py-2 border-transparent hover:border-blue-900 mr-4">
                            3/22/20
                        </button>                                        
                    </div>
                </div>
            </div> 
        </div>
        <div class="bg-white rounded border">
            <div>
                <table class="table-auto w-full">
                    <thead>
                        <tr class="text-left">
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400"></th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">aaaa</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Temp</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Client</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Hours</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">O/T</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Pay Rate</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Gross Pay</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Worked <br/>w/e date</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Expense<br/>Reimbursement</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">bill rate</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Invoice #</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Invoice $</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">Gross Profit</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">burden 15%</th>
                            <th class="text-xs font-medium px-2 py-1 bg-gray-400">net gross</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr> 
                            <td class="border px-1 py-1 text-center">
                                <input class="leading-tight" type="checkbox">
                            </td>                        
                            <td class="text-xs border px-1 py-1">bru 2</td>
                            <td class="text-xs border px-1 py-1">Doris Bruntel 2</td>
                            <td class="text-xs border px-1 py-1">Debevoise</td>
                            <td class="text-xs border px-1 py-1">2,5</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">$19,00</td>
                            <td class="text-xs border px-1 py-1">$47,50</td>
                            <td class="text-xs border px-1 py-1">15.04.18</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">30,00</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">$75,00</td>
                            <td class="text-xs border px-1 py-1">$27,50</td>
                            <td class="text-xs border px-1 py-1">$7,13</td>
                            <td class="text-xs border px-1 py-1">$20,38</td>                     
                        </tr>
                        <tr> 
                            <td class="border px-1 py-1 text-center">
                                <input class="leading-tight" type="checkbox">
                            </td>                        
                            <td class="text-xs border px-1 py-1">bru 2</td>
                            <td class="text-xs border px-1 py-1">Doris Bruntel 2</td>
                            <td class="text-xs border px-1 py-1">Debevoise</td>
                            <td class="text-xs border px-1 py-1">2,5</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">$19,00</td>
                            <td class="text-xs border px-1 py-1">$47,50</td>
                            <td class="text-xs border px-1 py-1">15.04.18</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">30,00</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">$75,00</td>
                            <td class="text-xs border px-1 py-1">$27,50</td>
                            <td class="text-xs border px-1 py-1">$7,13</td>
                            <td class="text-xs border px-1 py-1">$20,38</td>                     
                        </tr>
                        <tr> 
                            <td class="border px-1 py-1 text-center">
                                <input class="leading-tight" type="checkbox">
                            </td>                        
                            <td class="text-xs border px-1 py-1">bru 2</td>
                            <td class="text-xs border px-1 py-1">Doris Bruntel 2</td>
                            <td class="text-xs border px-1 py-1">Debevoise</td>
                            <td class="text-xs border px-1 py-1">2,5</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">$19,00</td>
                            <td class="text-xs border px-1 py-1">$47,50</td>
                            <td class="text-xs border px-1 py-1">15.04.18</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">30,00</td>
                            <td class="text-xs border px-1 py-1 text-center"><a href='#' class="text-blue-700 underline">7610</a></td>
                            <td class="text-xs border px-1 py-1">$75,00</td>
                            <td class="text-xs border px-1 py-1">$27,50</td>
                            <td class="text-xs border px-1 py-1">$7,13</td>
                            <td class="text-xs border px-1 py-1">$20,38</td>                     
                        </tr>
                        <tr>  
                            <td class="border px-1 py-1 text-center"></td>                       
                            <td class="text-xs border px-1 py-3"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>                     
                        </tr>
                        <tr class="font-bold">  
                            <td class="border px-1 py-1 text-center"></td>                       
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">Total</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">472,00</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">10766,13</td>
                            <td class="text-xs border px-1 py-1">$47,50</td>
                            <td class="text-xs border px-1 py-1">15.04.18</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">30,00</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">$75,00</td>
                            <td class="text-xs border px-1 py-1">$27,50</td>
                            <td class="text-xs border px-1 py-1">$7,13</td>
                            <td class="text-xs border px-1 py-1">$20,38</td>                     
                        </tr>
                        <tr> 
                            <td class="border px-1 py-1 text-center"></td>                        
                            <td class="text-xs border px-1 py-3"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>                     
                        </tr>
                        <tr class="font-medium"> 
                            <td class="border px-1 py-1 text-center"></td>                        
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">Employees</td>
                            <td class="text-xs border px-1 py-1">17</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>                     
                        </tr>
                        <tr class="font-medium">  
                            <td class="border px-1 py-1 text-center"></td>                       
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">Time Sheets</td>
                            <td class="text-xs border px-1 py-1">17</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>                     
                        </tr>
                        <tr class="font-medium">   
                            <td class="border px-1 py-1 text-center"></td>                      
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">Hours</td>
                            <td class="text-xs border px-1 py-1">487</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>                     
                        </tr>
                        <tr class="font-medium">    
                            <td class="border px-1 py-1 text-center"></td>                     
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1">Invoices</td>
                            <td class="text-xs border px-1 py-1">17</td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>
                            <td class="text-xs border px-1 py-1"></td>                     
                        </tr>                                                                                                                                                                                                                     
                    </tbody>
                </table>            
            </div>
        </div>
    </div>
@endsection